# Introdução

<p> O cidadão hoje é digital e tem acesso fácil a qualquer tipo informação ou serviço em sua palma da mão. Além disso, os governos têm o desafio de reduzir gastos com o atendimento presencial e, ao mesmo tempo, ser empático com as dificuldades e limitações do cidadão, oferecendo um novo conceito em qualidade de atendimento com base na experiência do usuário do serviço público. </p>

<p>Este Plano de Gerenciamento de Projeto contempla todo o planejamento do projeto Expresso Aplicações Usuários e aborda as áreas do conhecimento que são: escopo – cronograma – custo – qualidade – recursos humanos – riscos – aquisições – comunicação – partes interessadas, sendo ele representando integração, pois seu principal objetivo é integrar as demais em um plano conciso e coordenado.</p>

# **Benefícios Esperados**

1. Serviço ao cidadão
2. Painel do cidadão
3. Histórico de atendimento
4. Relatório de avaliação
5. Portal da Transparência
6. Perguntas Frequentes
7. Ouvidoria


# **Impacto Social**

<p>Todos os serviços listados podem ser solicitados através do Portal de Atendimento https://goias.gov.br/ ou diretamente deste documento clicando no nome da Solicitação que será direcionado a abertura do pedido.</p>

### EXPRESSO – A Estratégia de Transformação dos Serviços Públicos do Estado de Goiás 
![image](uploads/d7d018a0167807a8269adf1691fb5bdd/image.png)


# Integração de serviços tipo Redirect
A integração do tipo “Redirect” permite o acesso a um determinado serviço por meio de um redirecionamento do sistema expresso ao sistema do Órgão responsável. Este redirecionamento carrega um protocolo de atendimento que deverá ser utilizado para identificar o cidadão e registrar as etapas de atendimento internas ao serviço prestado. Abaixo podemos ver uma imagem que explica o fluxo de atendimento que deverá ocorrer com a cooperação dos sistemas.

![image](uploads/8cda581c1f6a90f8db262e7fcc14fdb5/image.png)

# Gerenciamento do Escopo 

## Escopos Expresso

<p>Para solicitar o consumo serviços agregados do expresso é necessário solicitar autorização para acesso a determinado escopo que deve estar atrelado a chave (clientID) do órgão. Atualmente os escopos existentes são: </p>

## Chave de Acesso (Expresso) - clientID

<p> Para realizar a integração de serviços no expresso é necessário habilitar uma chave de acesso. Essa chave é denominada clientID sendo essa única e intransferível por entidade que se deseja integrar no Expresso. Através dessa chave é feito o controle de acesso a funcionalidades como envio de sms, escopos entre outras. </p>

- **govbr** - Dados do gov.br (cpf, nome, celular, email, selos de confiabilidade e lista de empresas vinculadas). Por padrão todos aqueles se integram no expresso possuem esse escopo ativo

- **ip_geo** - Informações de IP e Geo Localização para fins de acesso a serviços

- **contato** - Dados de contato como email adicional e telefone

- **endereco** - Endereço autodeclarado do cidadão

- **dados_adicionais** - Dados adicionais de cadastro para evitar envio de comprovantes de endereço

# Estrutura Analítica do Projeto – EAP

![image](uploads/5d51892ecf1e2d2e2f2e8a405e9b2fde/image.png)

# Plano de Gerenciamento de Cronograma - PGTP

| Data | Entrega |Tarefa |Responsável |
| ------ | ------ |------ |------ |
| 21/04/2024 |26/07/2026 | Documentação    |João Carlos|
| 21/04/2024 |26/07/2026 |Desenvolvimento  |José Carlos|
| 21/04/2024 |26/07/2026 |Análise          |José Santos|

# Plano de Gerenciamento de Riscos - PGRI

<p> O Plano de Gerenciamento de Riscos visa a identificação e gerenciamento dos riscos, com intuito de aumentar a probabilidade e o impacto das oportunidades no projeto (eventos positivos), e reduzir a probabilidade e o impacto de ameaças (eventos negativos) bem como preparar respostas para tratá-los.
Segue abaixo a matriz de Probabilidade x Impacto dos riscos: </p>

![image](uploads/c9fd73969b2efd37b74288aead2e0b1e/image.png)
