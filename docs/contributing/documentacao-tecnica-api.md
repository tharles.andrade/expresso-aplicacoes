# Documento 

<p> Os documentos estão disponíveis para versões específicas da maioria das APIs. Cada documento descreve a superfície da API, como acessá-la e como as solicitações e respostas da API são estruturadas. As informações fornecidas pelo documento de descoberta incluem propriedades do nível da API, como uma descrição da API, esquemas de recursos, escopos de autenticação e métodos. </p>

## Cloud Login API

<p> Para chamar esse serviço, recomendamos usar as bibliotecas de cliente fornecidas pelo Google. Se seu aplicativo precisar usar suas próprias bibliotecas para chamar esse serviço, use as informações a seguir ao fazer as solicitações de API. </p>

## Métodos

<p> O documento no método RESTful de invocação de uma API. O método retorna a lista de todas as APIs compatíveis com o serviço de descoberta de APIs do Google, incluindo os URLs para recuperar os documentos de descoberta baseados em REST. </p>

## Representações de recursos

~~~javascript
{
  "kind": "discovery#restDescription",
  "discoveryVersion": "v1",
  "id": string,
  "name": string,
  "canonicalName": string,
  "version": string,
  "revision": string,
  "title": string,
  "description": string,
  "icons": {
    "x16": string,
    "x32": string
  },
  "documentationLink": string,
  "labels": [
    string
  ],
  "protocol": "rest",
  "baseUrl": string,
  "basePath": string,
  "rootUrl": string,
  "servicePath": string,
  "batchPath": "batch",
  "endpoints": [
    {
      "endpointUrl": string,
      "location": string,
      "deprecated": boolean,
      "description": string
    }
  ~~~

# Solicitação 

### Solicitação HTTP

`GET https://discovery.googleapis.com/discovery/v1/apis`

### Parâmetros

| Nome do parâmetro | Valor  | Descrição |
| ------            | ------ |------     |
| name              | string |Inclua somente APIs com o nome fornecido.|
| preferred         | boolean|Retornar somente a versão preferencial de uma API. "false" por padrão.|
     