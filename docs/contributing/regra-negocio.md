Descreve um aspecto do negócio, definindo ou restringindo tanto sua estrutura quanto seu comportamento. 

# Regras de Negócio

<details>
<summary>RN001 Data de Expiração</summary>

|Regra de Negócio          | Descrição|
| ------                   | ------   |
| **Data Expiração**          |Os usuários com data de expiração menor que a data atual terá seu acesso bloqueado no sistema.      | 
</details>

<details>
<summary>RN002 Login de Acesso</summary>

|Regra de Negócio          | Descrição|
| ------                   | ------   |
| **Acesso do Usuário**    |Somente usuários logados poderão visualizar o histórico de serviços | 
</details>

<details>
<summary>RN003 Acesso gov.br</summary>

|Regra de Negócio          | Descrição|
| ------                   | ------   |
| **Acesso do Usuário gov.br**    |É pré-requisito o usuário ter cadastro no gov.br para obter o acesso a partir da função gov.br. | 
</details>

<details>
<summary>RN004 Legislação Diretrizes</summary>

|Regra de Negócio          | Descrição|
| ------                   | ------   |
| **Decreto nº 10.058, de 18 de março DE 2022.**| XI – propor diretrizes para a gestão contínua da Carta de Serviços do Programa Expresso, com a orientação de cada órgão ou entidade da administração pública estadual sobre suas atribuições e responsabilidades, para aprimorar a clareza, a precisão e a qualidade das informações que estão disponíveis ao cidadão; | 
</details>

<details>
<summary>RN005 Tempo de Resposta</summary>

|Regra de Negócio          | Descrição|
| ------                   | ------   |
| **Tempo de resposta**    |O sistema deve ter o tempo de resposta dentro do para | 
</details>