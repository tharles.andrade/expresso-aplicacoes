# Tela de Acesso ao Serviço

<p>Estarão os serviços organizados pelo Núcleo de atividade, em seguida pelo escopo determinado pelo assunto, por fim a lista com as solicitações possíveis. Estas são direcionadas automaticamente para a equipe que realizará o atendimento.</p>

## I - Serviço ao cidadão

**A carta de serviços**
<p> Expõe de forma estruturada quais os serviços prestados pela, os critérios para a solicitação e atendimento e as dependências de cada serviço. A cadeia de valor integrada, também, facilita a identificação de competências e atribuições públicas, orientando o que precisa ser feito e, consequentemente, o que não precisa ser feito pelos entes públicos. Ela é indispensável no processo de transformação digital, pois corrobora com a identificação de todos os processos que entregam os serviços públicos estaduais de atendimento, facilitando a construção e qualificação de toda a base de conhecimento da transformação digital, a carta de serviços públicos. </p>

[![alttext](uploads/1033191cce9807003714b885a2efd65b/image.png)](https://goias.gov.br/)
_Figura 01 Tela de Serviços_

###  Pesquisa de Serviços
![image](uploads/3bba84723c3b97d3a1e14ccc8bdbc739/image.png)
_Figura 02 Pesquisa de Serviços_

### Personalização do Usuário
![image](uploads/d8e41021a05f918b9b80d472d00f9e10/image.png) <br>
_Figura 03 Personalização_

### Consulta de Serviços Diversos
![image](uploads/fba30fa58d282bfdf8e3c7a71bb693ab/image.png)
_Figura 04 Consulta de Serviços_


## II - Painel do cidadão

**Portal de Atendimento**
<p>Portal de atendimento é a plataforma oficial para abertura e tratamento das solicitações do usuário, bem como o meio principal para a comunicação com o solicitante dos serviços. Também pode ser solicitado telefone ou e-mail para possível impossibilidade de contato através da plataforma. Saiba como solicitar e acompanhar sua solicitação neste acessando este manual: Como solicitar e acompanhar minha solicitação</p>

![image](uploads/2d14df009b4cced7c09a2498d291d236/image.png)
_Figura 05 Painel do Cidadão_

## III - Histórico de atendimento

![image](uploads/e025e6f584d1095d19bde60cb41a22ee/image.png)
_Figura 06 Histórico de atendimento_
