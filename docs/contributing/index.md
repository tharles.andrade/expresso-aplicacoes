# Plataforma de Documentação 

Gestão de Documentos que reúne as documentações, com diferentes níveis de acessos. Contempla armazenamento, organização e distribuição inteligente dos documentos:

- **Técnica** Descrever o uso, funcionalidade ou arquitetura de um produto, sistema ou serviço.       
- **Portfólio do Projeto** Documentos mais relevantes por processo da visão do projeto.    
- **Base de Conhecimento** Instruções e orientações para a utilização de serviços, conjunto de instruções para obter conhecimento. 

# Técnica
Seção de documentações técnicas sobre o sistema, serviços e API.

[![alttext](uploads/b0d33d40ab1039e7e1af0a9d6e98f5fa/image.png)](https://codigo.dev.tools.go.gov.br/expresso-aplicacoes/expresso-aplicacoes-usuarios-api/-/wikis/home/Documenta%C3%A7%C3%A3o-T%C3%A9cnica-API)

# Portfólio do Projeto
Seção de documentação de processo e visão do projeto desenvolvido

[![alttext](uploads/a0b6a5c3d2f551eeeec502302cbe20ef/image.png)](https://codigo.dev.tools.go.gov.br/expresso-aplicacoes/expresso-aplicacoes-usuarios-api/-/wikis/Projeto-Expresso-Aplica%C3%A7%C3%B5es-Usu%C3%A1rios-(Modelo))

# Base de conhecimento 
Seção de documentação de apoio ao ambiente de aprendizagem e manuais

[![alttext](uploads/23678e4bb3a5fc156f240b23659faf20/image.png)](https://codigo.dev.tools.go.gov.br/expresso-aplicacoes/expresso-aplicacoes-usuarios-api/-/wikis/home/Manual-Do-Usu%C3%A1rio)

